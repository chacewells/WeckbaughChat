package cp125.week9;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author Scott
 * @date 06-02-2013
 * @version week9 Main chat utility
 */
public class Chat implements Runnable {
	/**
	 * fields
	 */
	
	public final static String status[] = {
			" Error connecting: verify there is a server instance.", " DISCONNECT",
			" Disconnecting...", " Connecting...", " Connected" };
	
	// constants
	public final static int NULL = 0;
	public final static int DISCONNECTED = 1;
	public final static int DISCONNECTING = 2;
	public final static int CONNECT = 3;
	public final static int CONNECTED = 4;
	public final static Chat chat = new Chat();
	public final static String END_SESSION = new Character((char) 0).toString(); 
	
	// Connection
	public static String host = "localhost";
	public static int port = 1234;
	public static int connectionStatus = DISCONNECTED;
	public static boolean isHost = true;
	public static String statusString = status[connectionStatus];
	public static StringBuffer toAppend = new StringBuffer("");
	public static StringBuffer toSend = new StringBuffer("");

	// TCP Components
	public static ServerSocket ss = null;
	public static Socket socket = null;
	public static BufferedReader in = null;
	public static PrintWriter out = null;
	static ExecutorService pool;
	
	static boolean exit(String txtInput3) {
		//TODO - I need to test multiple client instances (only tested client/server instance)
		if (txtInput3.equals("exit") || txtInput3.equals("quit")) {
			return true;
		} else
			return false;
	}

	/**
	 *  
	 * @return - I should have implemented this as a ChatUI object instead
	 * of using JPanel. This would have been cleaner but I had problems when 
	 * I started to add additional UI components. 
	 */
	public static JPanel createAndShowGUI() {
		Adaptor buttonL = null;
		// IP input
		// ChatUI.setupOptPane();
		ChatUI.optionPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		ChatUI.optionPane.add(new JLabel("IP:"));
		ChatUI.ipTxt = new JTextField(10);
		ChatUI.ipTxt.setText(host);
		ChatUI.ipTxt.setEnabled(false);
		/**
		 * Set focus on ip text
		 */
		ChatUI.ipTxt.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				ChatUI.ipTxt.selectAll();
				if (connectionStatus != DISCONNECTED) {
					alterGUIState(NULL, true);
				} else {
					host = ChatUI.ipTxt.getText();
				}
			}
		});
		ChatUI.optionPane.add(ChatUI.ipTxt);
		ChatUI.optPane.add(ChatUI.optionPane);
		// Port input
		ChatUI.optionPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		ChatUI.optionPane.add(new JLabel("Port:"));
		ChatUI.portTxt = new JTextField(10);
		ChatUI.portTxt.setEditable(true);
		ChatUI.portTxt.setText((new Integer(port)).toString());
		
		/**
		 * Set focus on port text
		 */
		ChatUI.portTxt.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				// should be editable only when DISCONNECT
				if (connectionStatus != DISCONNECTED) {
					alterGUIState(NULL, true);
				} else {
					int i;
					try {
						i = Integer.parseInt(ChatUI.portTxt.getText());
						port = i;
					} catch (NumberFormatException nfe) {
						ChatUI.portTxt.setText((new Integer(port)).toString());
						ChatUI.parentFrame.repaint();
					}
				}
			}
		});
		ChatUI.optionPane.add(ChatUI.portTxt);
		ChatUI.optPane.add(ChatUI.optionPane);

		/**
		 * actionPerformed on enabling ip
		 */
		buttonL = new Adaptor() {
			public void actionPerformed(ActionEvent e) {
				if (connectionStatus != DISCONNECTED) {
					alterGUIState(NULL, true);
				} else {
					isHost = e.getActionCommand().equals("host");
					if (isHost) {
						ChatUI.ipTxt.setEnabled(false);
						ChatUI.ipTxt.setText("localhost");
						host = "localhost";
					} else {
						ChatUI.ipTxt.setEnabled(true);
					}
				}
			}
		};

		ChatUI.hostOpt = new JRadioButton("Server", true);
		ChatUI.hostOpt.setMnemonic(KeyEvent.VK_H);
		ChatUI.hostOpt.setActionCommand("Server");
		ChatUI.hostOpt.addActionListener(buttonL);
		ChatUI.clientOpt = new JRadioButton("Client", false);
		ChatUI.clientOpt.setMnemonic(KeyEvent.VK_G);
		ChatUI.clientOpt.setActionCommand("client");
		ChatUI.clientOpt.addActionListener(buttonL);
		ChatUI.setHostOption();
		ChatUI.setClientOption();
		ChatUI.optionPane = new JPanel(new GridLayout(1, 2));
		ChatUI.optionPane.add(ChatUI.hostOpt);
		ChatUI.optionPane.add(ChatUI.clientOpt);
		ChatUI.optPane.add(ChatUI.optionPane);

		/**
		 * actionPerformed on request connection
		 */
		buttonL = new Adaptor() {
			public void actionPerformed(ActionEvent e) {
				// Request a connection initiation
				if (e.getActionCommand().equals("connect")) {
					alterGUIState(CONNECT, true);
				}
				// Disconnect
				else {
					alterGUIState(DISCONNECTING, true);
				}
			}
		};
		
		ChatUI.connectButton = new JButton("Connect");
		ChatUI.connectButton.setMnemonic(KeyEvent.VK_C);
		ChatUI.connectButton.setActionCommand("connect");
		ChatUI.connectButton.addActionListener(buttonL);
		ChatUI.connectButton.setEnabled(true);
		ChatUI.disconnectButton = new JButton("Disconnect");
		ChatUI.disconnectButton.setMnemonic(KeyEvent.VK_D);
		ChatUI.disconnectButton.setActionCommand("disconnect");
		ChatUI.disconnectButton.addActionListener(buttonL);
		ChatUI.disconnectButton.setEnabled(false);
		ChatUI.buttonPane.add(ChatUI.connectButton);
		ChatUI.buttonPane.add(ChatUI.disconnectButton);
		ChatUI.optPane.add(ChatUI.buttonPane);

		return ChatUI.optPane;
	}

	/**
	 * 
	 * @param newConnectStatus
	 * @param noError
	 */
	private static void alterGUIState(int newConnectStatus, boolean noError) {
		if (newConnectStatus != NULL) {
			connectionStatus = newConnectStatus;
		}

		if (noError) {
			statusString = status[connectionStatus];
		}

		else {
			statusString = status[NULL];
		}
		// Swing Threading Policy rule which states that all UI updates must be
		// done on the UI event thre
		SwingUtilities.invokeLater(chat);
	}

	/**
	 * setting chat toSend
	 */
	static void send(String st) {
		synchronized (Chat.toSend) {
		Chat.toSend.append(st + "\n");
		}
	}

	/**
	 * Append chat - using safe thread synchronized for the toAppend StringBuffer
	 */
	public static void appendChat(String s) {
		synchronized (toAppend) {
			toAppend.append(s);
		}
	}

	/**
	 * Cleanup
	 */
	private static void close() {
		try {
			if (ss != null) {
				ss.close();
				ss = null;
			}
		} catch (IOException e) {
			ss = null;
		}

		try {
			if (socket != null) {
				socket.close();
				socket = null;
			}
		} catch (IOException e) {
			socket = null;
		}

		try {
			if (in != null) {
				in.close();
				in = null;
			}
		} catch (IOException e) {
			in = null;
		}

		if (out != null) {
			out.close();
			out = null;
		}
	}

	/**
	 * Running and setting ConnectionStatus
	 */
	public void run() {
		switch (connectionStatus) {
		case DISCONNECTED:
			ChatUI.disconnectButton.setEnabled(true);
			ChatUI.connectButton.setEnabled(false);
			ChatUI.ipTxt.setEnabled(true);
			ChatUI.portTxt.setEnabled(true);
			ChatUI.hostOpt.setEnabled(true);
			ChatUI.clientOpt.setEnabled(true);
			ChatUI.chatLine.setText("");
			ChatUI.chatLine.setEnabled(false);
			ChatUI.statusColor.setBackground(Color.red);
			break;

		case DISCONNECTING:
			ChatUI.connectButton.setEnabled(false);
			ChatUI.disconnectButton.setEnabled(false);
			ChatUI.ipTxt.setEnabled(false);
			ChatUI.portTxt.setEnabled(false);
			ChatUI.hostOpt.setEnabled(false);
			ChatUI.clientOpt.setEnabled(false);
			ChatUI.chatLine.setEnabled(false);
			ChatUI.statusColor.setBackground(Color.orange);
			break;

		case CONNECTED:
			ChatUI.connectButton.setEnabled(false);
			ChatUI.disconnectButton.setEnabled(true);
			ChatUI.ipTxt.setEnabled(false);
			ChatUI.portTxt.setEnabled(false);
			ChatUI.hostOpt.setEnabled(false);
			ChatUI.clientOpt.setEnabled(false);
			ChatUI.chatLine.setEnabled(true);
			ChatUI.statusColor.setBackground(Color.green);
			break;

		case CONNECT:
			ChatUI.connectButton.setEnabled(false);
			ChatUI.disconnectButton.setEnabled(false);
			ChatUI.ipTxt.setEnabled(false);
			ChatUI.portTxt.setEnabled(false);
			ChatUI.hostOpt.setEnabled(false);
			ChatUI.clientOpt.setEnabled(false);
			ChatUI.chatLine.setEnabled(false);
			ChatUI.chatLine.grabFocus();
			ChatUI.statusColor.setBackground(Color.orange);
			break;
		}

		ChatUI.ipTxt.setText(host);
		ChatUI.portTxt.setText((new Integer(port)).toString());
		ChatUI.hostOpt.setSelected(isHost);
		ChatUI.clientOpt.setSelected(!isHost);
		ChatUI.statusLabel.setText(statusString);
		ChatUI.chatTxtArea.append(toAppend.toString());
		toAppend.setLength(0);

		ChatUI.parentFrame.repaint();
	}

	/**
	 * Add actionListener for outbound
	 */
	public static void local() {
		ChatUI.chatLine.addActionListener(new Adaptor() {
			public void actionPerformed(ActionEvent e) {
				String st = ChatUI.chatLine.getText();
				if (exit(st)) {						
					System.out.println("Adios amigo!");
					//TODO: figure out how to shutdown the pool 
					//getting a null exception after exiting the last session					
					//pool.shutdown();
					System.exit(0);
				} else if (!st.equals("")) {
					appendChat("OUTBOUND: " + st + "\n");
					ChatUI.chatLine.selectAll();

					// Send the string
					send(st);
				}
			}
		});
	}

	/**
	 * Pooling the thread for the socket
	 * @param socket
	 */
	private static void start(Socket socket) {
		pool = Executors.newFixedThreadPool(2);
	}
	
	/**
	 * Connection setup for inbound
	 */
	public static void remote() {
		String s;
		while (true) {
			try { // sleep every 10 mili seconds
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}

			switch (connectionStatus) {
			case CONNECT:
				try {
					// Run server if host
					if (isHost) {
						ss = new ServerSocket(port);
						socket = ss.accept();
					}

					// If client, try to connecting to the server
					else {
						socket = new Socket(host, port);
						start(socket);
					}

					in = new BufferedReader(new InputStreamReader(socket
							.getInputStream()));
					out = new PrintWriter(socket.getOutputStream(), true);
					alterGUIState(CONNECTED, true);
				}
				// If error, clean up and output an error message
				catch (IOException e) {
					close();
					alterGUIState(DISCONNECTED, false);
				}
				break;

			case CONNECTED:
				try {
					// Send data
					if (toSend.length() != 0) {
						out.print(toSend);
						out.flush();
						toSend.setLength(0);
						alterGUIState(NULL, true);
					}

					if (in.ready()) {
						s = in.readLine();
						if ((s != null) && (s.length() != 0)) {

							if (s.equals(END_SESSION)) {
								alterGUIState(DISCONNECTING, true);
							}

							else {
								appendChat("INBOUND: " + s + "\n");
								alterGUIState(NULL, true);
							}
						}
					}
				} catch (IOException e) {
					close();
					alterGUIState(DISCONNECTED, false);
				}
				break;

			case DISCONNECTING:
				//TODO: fix the null pointer bug when clicking the Disconnect button a second time
				out.print(END_SESSION);
				out.flush();
				close();
				alterGUIState(DISCONNECTED, true);
				break;

			default:
				break;
			}
		}
	}

	/**
	 * Simple main to execute program
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
		}
		ChatUI.setupGUI();
		local();
		remote();

	}
	
}

/**
 * 
 * @author chq-scottw Adaptor implements the ActionListener interface 
 * 
 */

class Adaptor implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	}
}
