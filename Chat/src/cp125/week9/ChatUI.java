package cp125.week9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;
/**
 * 
 * @author Scott Weckbaugh
 * @date 06-02-2013
 * @version week9 Chat class to establish, define, and setup the UI
 */
public class ChatUI {
	// Various GUI components and info
	/**
	 * Fields
	 */
	public static JFrame parentFrame = null;
	public static JTextArea chatTxtArea = null;
	public static JTextField chatLine = null;
	public static JPanel statusPanel = null;
	public static JLabel statusLabel = null;
	public static JTextField statusColor = null;
	public static JTextField ipTxt = null;
	public static JTextField portTxt = null;
	public static JRadioButton hostOpt = null;
	public static JRadioButton clientOpt = null;
	public static JButton connectButton = null;
	public static JButton disconnectButton = null;
	public static JPanel optionPane = null;
	public static JPanel buttonPane = new JPanel(new GridLayout(1, 2));
	public static ButtonGroup buttonGroup = new ButtonGroup();
	public static JPanel optPane = new JPanel(new GridLayout(4, 1));

	// Options
	/**
	 * Setup the client option button
	 */
	public static StringBuffer toAppend = new StringBuffer("");
	static public void setClientOption() {
		buttonGroup.add(clientOpt);
	}
	
	/**
	 * Setup the host option button
	 */
	static public void setHostOption() {
		buttonGroup.add(hostOpt);
	}
	
	/**
	 * Status Message
	 */
	public final static String statusMsg[] = {
			" Error connecting: verify there is a server instance.", " DISCONNECT",
			" Disconnecting...", " Connecting...", " Connected" };

	/**
	 * GUI setup, positioning and configuration
	 */
	public static void setupGUI() {
		statusLabel = new JLabel();
		statusLabel.setText(statusMsg[Chat.DISCONNECTED]);
		statusColor = new JTextField(1);
		statusColor.setBackground(Color.red);
		statusColor.setEditable(false);
		statusPanel = new JPanel(new BorderLayout());
		JPanel optPane = Chat.createAndShowGUI();
		JPanel chatPane = new JPanel(new BorderLayout());
		chatTxtArea = new JTextArea(15, 20);
		chatTxtArea.setLineWrap(true);
		chatTxtArea.setEditable(false);
		chatTxtArea.setForeground(Color.blue);
		JScrollPane chatTxtPane = new JScrollPane(chatTxtArea,
		JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		chatLine = new JTextField();
		chatLine.setEnabled(false);
		chatPane.setPreferredSize(new Dimension(300, 300));
		JPanel parentPane = new JPanel(new BorderLayout());
		parentPane.add(statusPanel, BorderLayout.SOUTH);
		parentPane.add(optPane, BorderLayout.WEST);
		parentPane.add(chatPane, BorderLayout.NORTH);
		statusPanel.add(statusColor, BorderLayout.WEST);
		statusPanel.add(statusLabel, BorderLayout.CENTER);
		chatPane.add(chatLine, BorderLayout.NORTH);
		chatPane.add(chatTxtPane, BorderLayout.SOUTH);

		// Set up the parent frame
		parentFrame = new JFrame("Chat");
		parentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		parentFrame.setContentPane(parentPane);
		parentFrame.setIconImage(new ImageIcon("ChatIcon.jpg").getImage());
		parentFrame.setResizable(false);
		parentFrame.setSize(parentFrame.getPreferredSize());
		parentFrame.setLocation(300, 300);
		parentFrame.pack();
		parentFrame.setVisible(true);
	}

}
